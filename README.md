

> **# Recipe-Book** 

Cook delicious vegetarian food at home with step by step photos. App sharing 1400 recipes from the most popular Indian recipe website. All recipes are tried and tested by us as well as by our readers. If you want to learn Indian vegetarian cooking then do give this App a try as we include a lot of photos in our step by step recipe format. The App has many good features - like works in offline mode (make the recipes favorite for it to work in offline mode). We are now also slowly adding nutrition info to the recipes.

For ease of readers recipes in this App are divided into various categories:

1. Indian Breakfast Recipes
2. Rice Recipes
3. Main Course Recipes, etc...

[See More: ](https://play.google.com/store/apps/details?id=com.app.recepiesbook.recipes)


**# Installation**

**# OS X**
Download RecipeBook-osx.x.x.x.dmg or RecipeBook-osx.x.x.x.zip.

Open or unzip the file and drag the app into the Applications folder.

Done!

**# Windows**

Download or Clone RecipeBook-osx.x.x.x.zip.

Open or unzip the file and Extract the Zip into the your Application folder.

Done!

**# Linux**

Ubuntu, Debian 8+ (deb package):

Download RecipeBook-linux.x.x.x.deb.

Double click and install, or run dpkg -i RecipeBook-linux.x.x.x.deb in the terminal.

Start the app with your app launcher or by running RecipeBook in a terminal.

**# For developers**

Clone the source locally:

$ git clone https://gitlab.com/SejalBaraiya/recipebook $ cd RecipeBook

**# Features**

Offline support

Cross-platform

Awesome Recipes

No singup/login required

Auto launch

Auto updates

**# Built- in**

Android Studio

Firebase Database

Firebase integration

New Animations with CardView

Custom Components


